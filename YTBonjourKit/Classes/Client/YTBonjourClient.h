//
//  YTBonjourClient.h
//  YTBonjourKit
//
//  Created by aron on 2020/8/13.
//

#import <Foundation/Foundation.h>
#import "YTNetServiceModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YTBonjourClient : NSObject

/// 获取可用的服务
@property (nonatomic, strong, readonly) NSMutableArray<YTNetServiceModel *> *availableService;

+ (instancetype)sharedInstance;

/// 开始发现服务
- (void)startFindService;

/// 停止发现服务
- (void)stopFindService;

@end

NS_ASSUME_NONNULL_END
