//
//  YTServerDemoViewController.h
//  YTBonjourKit_Example
//
//  Created by aron on 2020/8/13.
//  Copyright © 2020 aron. All rights reserved.
//

#import "BBTKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YTServerDemoViewController : BBTKBaseViewController

@end

NS_ASSUME_NONNULL_END
