//
//  BBTKBaseViewController.m
//  EffectiveOCDemo
//
//  Created by aron on 2017/4/18.
//  Copyright © 2017年 aron. All rights reserved.
//

#import "BBTKBaseViewController.h"
#import "BBTKUIUtil.h"

@implementation BBTKActionCellModel

-(instancetype)initWithActionName:(NSString*)actionName actionCallBack:(void(^)(void))callback {
    if (self = [super init]) {
        _actionName = actionName;
        _actionCallBack = callback;
    }
    return self;
}

@end



@interface BBTKBaseViewController ()

@property (nonatomic, strong) NSMutableArray* actionCellModels;

@end

@implementation BBTKBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITableView* tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    self.tableView = tableView;

    _actionCellModels = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 添加一个Cell和Cell对应的点击事件回调block
 @param title Cell显示名称
 @param callback 点击事件回调block
 */
- (void)bbtk_addActionWithTitle:(NSString *)title callback:(void(^)(void))callback {
    [self bbtk_addActionWithTitle:title detailText:nil callback:callback];
}

- (void)bbtk_addActionWithTitle:(NSString *)title detailText:(NSString *)detailText callback:(void(^)(void))callback {
    BBTKActionCellModel* model = [[BBTKActionCellModel alloc] initWithActionName:title actionCallBack:^{
        !callback ?: callback();
    }];
    model.detailText = detailText;
    [_actionCellModels addObject:model];
}

/**
 添加一个Cell和Cell对应的点击跳转到的ViewController的class
 @param destViewControllClass 转到的ViewController的class
 */
- (void)bbtk_addActionWithDestViewControllClass:(Class)destViewControllClass {
    [self bbtk_addActionWithTitle:NSStringFromClass(destViewControllClass) destViewControllClass:destViewControllClass];
}

/**
 添加一个Cell和Cell对应的点击跳转到的ViewController的class
 @param detailText 描述文字
 @param destViewControllClass 转到的ViewController的class
 */
- (void)bbtk_addActionWithDetailText:(NSString *)detailText destViewControllClass:(Class)destViewControllClass {
    [self bbtk_addActionWithTitle:NSStringFromClass(destViewControllClass) detailText:detailText destViewControllClass:destViewControllClass];
}

/**
 添加一个Cell和Cell对应的点击跳转到的ViewController的class
 @param title Cell显示名称
 @param destViewControllClass 转到的ViewController的class
 */
- (void)bbtk_addActionWithTitle:(NSString *)title destViewControllClass:(Class)destViewControllClass {
    [self bbtk_addActionWithTitle:title detailText:nil destViewControllClass:destViewControllClass];
}

- (void)bbtk_addActionWithTitle:(NSString *)title detailText:(NSString *)detailText destViewControllClass:(Class)destViewControllClass {
    BBTKActionCellModel* model = [[BBTKActionCellModel alloc] initWithActionName:title actionCallBack:^{
        UIViewController* vc = [[destViewControllClass alloc] init];
        vc.title = title;
        if ([vc isKindOfClass:[UIViewController class]]) {
            [BBTKUIUtil pushViewController:vc animated:YES];
        }
    }];
    model.detailText = detailText;
    [_actionCellModels addObject:model];
}

#pragma mark - ......::::::: UITableViewDelegate :::::::......

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _actionCellModels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(UITableViewCell.class)];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NSStringFromClass(UITableViewCell.class)];
    }
    BBTKActionCellModel* model = _actionCellModels[indexPath.row];
    cell.textLabel.text = model.actionName;
    if (model.detailText) {
        cell.detailTextLabel.text = model.detailText;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    BBTKActionCellModel* model = _actionCellModels[indexPath.row];
    !model.actionCallBack ?: model.actionCallBack();
}
@end
