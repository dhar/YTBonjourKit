# BBTestKit

[![CI Status](https://img.shields.io/travis/aron/BBTestKit.svg?style=flat)](https://travis-ci.org/aron/BBTestKit)
[![Version](https://img.shields.io/cocoapods/v/BBTestKit.svg?style=flat)](https://cocoapods.org/pods/BBTestKit)
[![License](https://img.shields.io/cocoapods/l/BBTestKit.svg?style=flat)](https://cocoapods.org/pods/BBTestKit)
[![Platform](https://img.shields.io/cocoapods/p/BBTestKit.svg?style=flat)](https://cocoapods.org/pods/BBTestKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BBTestKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BBTestKit'
```

## Author

aron, zhangyutang@babybus.com

## License

BBTestKit is available under the MIT license. See the LICENSE file for more info.
