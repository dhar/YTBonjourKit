//
//  main.m
//  YTBonjourKit
//
//  Created by aron on 08/13/2020.
//  Copyright (c) 2020 aron. All rights reserved.
//

@import UIKit;
#import "YTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YTAppDelegate class]));
    }
}
