//
//  YTAppDelegate.h
//  YTBonjourKit
//
//  Created by aron on 08/13/2020.
//  Copyright (c) 2020 aron. All rights reserved.
//

@import UIKit;

@interface YTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
