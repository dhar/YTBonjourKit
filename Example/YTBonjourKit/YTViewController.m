//
//  YTViewController.m
//  YTBonjourKit
//
//  Created by aron on 08/13/2020.
//  Copyright (c) 2020 aron. All rights reserved.
//

#import "YTViewController.h"
#import <YTBonjourKit/YTBonjourServer.h>
#import <YTBonjourKit/YTBonjourClient.h>
#import "YTBonjourServerListViewController.h"
#import "YTServerDemoViewController.h"

@interface YTViewController ()
@property (nonatomic, strong) YTBonjourServer *server;
@property (nonatomic, strong) YTBonjourClient *client;

@end

@implementation YTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self bbtk_addActionWithTitle:@"Server Demo" destViewControllClass:YTServerDemoViewController.class];

    [self bbtk_addActionWithTitle:@"StartClient" callback:^{
        self.client = [YTBonjourClient sharedInstance];
        [self.client startFindService];
    }];
    
    [self bbtk_addActionWithTitle:@"Server List" destViewControllClass:YTBonjourServerListViewController.class];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
