//
//  YTNetServiceModel.h
//  YTBonjourKit
//
//  Created by aron on 2020/8/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YTNetServiceModel : NSObject

@property (nonatomic, strong, readonly) NSInputStream *input;
@property (nonatomic, strong, readonly) NSOutputStream *output;

@property (nonatomic, copy) NSString *serviceName;
@property (nonatomic, strong) NSNetService *netService;

- (void)sendText:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
