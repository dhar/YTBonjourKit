//
//  YTServerDemoViewController.m
//  YTBonjourKit_Example
//
//  Created by aron on 2020/8/13.
//  Copyright © 2020 aron. All rights reserved.
//

#import "YTServerDemoViewController.h"
#import <YTBonjourKit/YTBonjourServer.h>

@interface YTServerDemoViewController ()

@end

@implementation YTServerDemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self bbtk_addActionWithTitle:@"Start Server" callback:^{
        [[YTBonjourServer sharedInstance] startServer];
    }];
    
    [self bbtk_addActionWithTitle:@"Stop Server" callback:^{
        [[YTBonjourServer sharedInstance] stopServer];
    }];
}


@end
