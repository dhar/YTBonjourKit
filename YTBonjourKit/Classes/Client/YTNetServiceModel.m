//
//  YTNetServiceModel.m
//  YTBonjourKit
//
//  Created by aron on 2020/8/13.
//

#import "YTNetServiceModel.h"

@interface YTNetServiceModel ()

@property (nonatomic, strong) NSInputStream *input;
@property (nonatomic, strong) NSOutputStream *output;

@end

@implementation YTNetServiceModel

// MARK: - Public

- (void)sendText:(NSString *)text {
    [self.output open];
    [self.output write:(const uint8_t *)[text UTF8String] maxLength:text.length];
}


// MARK: - Getter

- (NSOutputStream *)output {
    if (!_output) {
        [self.netService getInputStream:&_input outputStream:&_output];
    }
    return _output;
}

- (NSInputStream *)input {
    if (!_input) {
        [self.netService getInputStream:&_input outputStream:&_output];
    }
    return _input;
}

@end
