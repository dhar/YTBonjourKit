# YTBonjourKit

[![CI Status](https://img.shields.io/travis/aron/YTBonjourKit.svg?style=flat)](https://travis-ci.org/aron/YTBonjourKit)
[![Version](https://img.shields.io/cocoapods/v/YTBonjourKit.svg?style=flat)](https://cocoapods.org/pods/YTBonjourKit)
[![License](https://img.shields.io/cocoapods/l/YTBonjourKit.svg?style=flat)](https://cocoapods.org/pods/YTBonjourKit)
[![Platform](https://img.shields.io/cocoapods/p/YTBonjourKit.svg?style=flat)](https://cocoapods.org/pods/YTBonjourKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YTBonjourKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'YTBonjourKit'
```

## Author

aron, zhangyutang@babybus.com

## License

YTBonjourKit is available under the MIT license. See the LICENSE file for more info.
