//
//  YTBonjourServerListViewController.m
//  YTBonjourKit_Example
//
//  Created by aron on 2020/8/13.
//  Copyright © 2020 aron. All rights reserved.
//

#import "YTBonjourServerListViewController.h"
#import <YTBonjourKit/YTNetServiceModel.h>
#import <YTBonjourKit/YTBonjourClient.h>

@interface YTBonjourServerListViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) UITableView* tableView;

@property (nonatomic, strong) NSArray *datas;

@end

@implementation YTBonjourServerListViewController

- (void)viewDidLoad {
    UITableView* tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    self.datas = [YTBonjourClient sharedInstance].availableService;
}

#pragma mark - ......::::::: UITableViewDelegate :::::::......

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(UITableViewCell.class)];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NSStringFromClass(UITableViewCell.class)];
    }
    YTNetServiceModel *data = self.datas[indexPath.row];
    cell.textLabel.text = data.serviceName;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    YTNetServiceModel *model = self.datas[indexPath.row];
    [model sendText:@"Hello World"];
}

@end
