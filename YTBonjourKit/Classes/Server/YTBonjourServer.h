//
//  YTBonjourServer.h
//  YTBonjourKit
//
//  Created by aron on 2020/8/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YTBonjourServer : NSObject

+ (instancetype)sharedInstance;

/// 开启服务
- (void)startServer;

/// 停止服务
- (void)stopServer;

@end

NS_ASSUME_NONNULL_END
