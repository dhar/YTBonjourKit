//
//  BBTKBaseViewController.h
//  EffectiveOCDemo
//
//  Created by aron on 2017/4/18.
//  Copyright © 2017年 aron. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ActionCellType) {
    ActionCellTypeNone = -1,
    ActionCellTypeCustomCallback = 0,
    ActionCellTypePushViewController = 1,
};

@interface BBTKActionCellModel : NSObject

@property (nonatomic, copy) NSString* actionName;
@property (nonatomic, copy) NSString* detailText;
@property (nonatomic, copy) void(^actionCallBack)(void);
@property (nonatomic, assign) ActionCellType actionCellType;

-(instancetype)initWithActionName:(NSString*)actionName actionCallBack:(void(^)(void))callback;

@end


@interface BBTKBaseViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) UITableView* tableView;

/**
 添加一个Cell和Cell对应的点击事件回调block
 @param title Cell显示名称
 @param callback 点击事件回调block
 */
- (void)bbtk_addActionWithTitle:(NSString *)title callback:(void(^)(void))callback;

/**
 添加一个Cell和Cell对应的点击事件回调block

 @param title Cell显示名称
 @param detailText 描述文字
 @param callback 击事件回调block
 */
- (void)bbtk_addActionWithTitle:(NSString *)title detailText:(NSString *)detailText callback:(void(^)(void))callback;

/**
 添加一个Cell和Cell对应的点击跳转到的ViewController的class
 @param destViewControllClass 转到的ViewController的class
 */
- (void)bbtk_addActionWithDestViewControllClass:(Class)destViewControllClass;

/**
 添加一个Cell和Cell对应的点击跳转到的ViewController的class
 @param detailText 描述文字
 @param destViewControllClass 转到的ViewController的class
 */
- (void)bbtk_addActionWithDetailText:(NSString *)detailText destViewControllClass:(Class)destViewControllClass;

/**
 添加一个Cell和Cell对应的点击跳转到的ViewController的class
 @param title Cell显示名称
 @param destViewControllClass 转到的ViewController的class
 */
- (void)bbtk_addActionWithTitle:(NSString *)title destViewControllClass:(Class)destViewControllClass;

/**
 添加一个Cell和Cell对应的点击跳转到的ViewController的class
 @param title Cell显示名称
 @param detailText 描述文字
 @param destViewControllClass 转到的ViewController的class
 */
- (void)bbtk_addActionWithTitle:(NSString *)title detailText:(NSString *)detailText destViewControllClass:(Class)destViewControllClass;
@end
